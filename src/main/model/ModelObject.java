package main.model;

import lwjglutils.OGLBuffers;
import lwjglutils.OGLTexture2D;
import main.utils.AbstractRenderer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL12.GL_CLAMP_TO_EDGE;

public class ModelObject extends AbstractRenderer {

    public static void modelLongBuilding(OGLTexture2D texture) {
        modelCuboid(5, 3, 1, texture);
        modelCube(5, 3.1f, 1);
        glMatrixMode(GL_MODELVIEW);
    }

    public static void modelHeighBuilding(OGLTexture2D texture) {
        modelCuboid(1, 5, 1, texture);
        modelCube(1, 5.1f, 1);
        glMatrixMode(GL_MODELVIEW);
    }


    public static void modelCuboid(double sizeX, double sizeY, double sizeZ, OGLTexture2D texture) {

        double[][] n = {
                {-1.0, 0.0, 0.0},
                {0.0, 1.0, 0.0},
                {1.0, 0.0, 0.0},
                {0.0, -1.0, 0.0},
                {0.0, 0.0, 1.0},
                {0.0, 0.0, -1.0}
        };
        int[][] faces = {
                {0, 1, 2, 3},
                {3, 2, 6, 7},
                {7, 6, 5, 4},
                {4, 5, 1, 0},
                {5, 6, 2, 1},
                {7, 4, 0, 3}
        };
        float[][] v = new float[8][3];
        v[0][0] = v[1][0] = v[2][0] = v[3][0] = (float) -sizeX / 2;
        v[4][0] = v[5][0] = v[6][0] = v[7][0] = (float) sizeX / 2;
        v[0][1] = v[1][1] = v[4][1] = v[5][1] = (float) -sizeY / 2;
        v[2][1] = v[3][1] = v[6][1] = v[7][1] = (float) sizeY / 2;
        v[0][2] = v[3][2] = v[4][2] = v[7][2] = (float) -sizeZ / 2;
        v[1][2] = v[2][2] = v[5][2] = v[6][2] = (float) sizeZ / 2;

        for (int i = 5; i >= 0; i--) {

            glMatrixMode(GL_TEXTURE);
            glPopMatrix();

            texture.bind();
            glBegin(GL_QUADS);
            glPopMatrix();
            glNormal3dv(n[i]);
            glTexCoord2f(1f, 0f);
            glVertex3fv(v[faces[i][0]]);
            glTexCoord2f(1f, 1f);
            glVertex3fv(v[faces[i][1]]);
            glTexCoord2f(0f, 1f);
            glVertex3fv(v[faces[i][2]]);
            glTexCoord2f(0f, 0f);
            glVertex3fv(v[faces[i][3]]);
            glPushMatrix();
            glEnd();
        }
        texture.bind();
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

        texture.bind();
        //static object with static scaled texture
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glTranslatef(0, 0f, 0f);
        glMatrixMode(GL_TEXTURE);
        glLoadIdentity();
        glScalef(1, 1, 1);

        OGLBuffers.Attrib[] attributes = {
                new OGLBuffers.Attrib("inPosition", 3),
                new OGLBuffers.Attrib("inNormal", 3),
                new OGLBuffers.Attrib("inTextureCoordinates", 2)
        };
    }

    public static void modelCube(double sizeX, double sizeY, double sizeZ) {
        double[][] n = {
                {-1.0, 0.0, 0.0},
                {0.0, 1.0, 0.0},
                {1.0, 0.0, 0.0},
                {0.0, -1.0, 0.0},
                {0.0, 0.0, 1.0},
                {0.0, 0.0, -1.0}
        };
        int[][] faces = {
                {0, 1, 2, 3},
                {3, 2, 6, 7},
                {7, 6, 5, 4},
                {4, 5, 1, 0},
                {5, 6, 2, 1},
                {7, 4, 0, 3}
        };
        float[][] v = new float[8][3];
        v[0][0] = v[1][0] = v[2][0] = v[3][0] = (float) -sizeX / 2;
        v[4][0] = v[5][0] = v[6][0] = v[7][0] = (float) sizeX / 2;
        v[0][1] = v[1][1] = v[4][1] = v[5][1] = (float) -sizeY / 2;
        v[2][1] = v[3][1] = v[6][1] = v[7][1] = (float) sizeY / 2;
        v[0][2] = v[3][2] = v[4][2] = v[7][2] = (float) -sizeZ / 2;
        v[1][2] = v[2][2] = v[5][2] = v[6][2] = (float) sizeZ / 2;

        for (int i = 5; i >= 0; i--) {
            glBegin(GL_QUADS);
            glNormal3dv(n[i]);
            glVertex3fv(v[faces[i][0]]);
            glVertex3fv(v[faces[i][1]]);
            glVertex3fv(v[faces[i][2]]);
            glVertex3fv(v[faces[i][3]]);
            glEnd();
        }
    }

}
