package main.model;

import lwjglutils.OGLTexture2D;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11.glScalef;
import static org.lwjgl.opengl.GL12.GL_CLAMP_TO_EDGE;

public class Textures {
    public static void modelTextures(OGLTexture2D texture) {
        texture.bind();
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);


        //static object with static scaled texture
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glTranslatef(0, 0f, 0f);
        glMatrixMode(GL_TEXTURE);
        glLoadIdentity();
        glScalef(1f, 1f, 1f);
    }

}
