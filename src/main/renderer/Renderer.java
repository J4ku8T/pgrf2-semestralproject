package main.renderer;


import lwjglutils.*;
import main.controller.GLCamera;
import main.utils.AbstractRenderer;
import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import org.lwjgl.glfw.GLFWScrollCallback;
import transforms.Vec3D;

import java.io.IOException;
import java.nio.DoubleBuffer;

import static main.utils.GluUtils.gluPerspective;
import static main.model.ModelObject.*;
import static main.model.Textures.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;


public class Renderer extends AbstractRenderer {
    private float mouseX, mouseY;
    private boolean mouseButton1 = false;
    private boolean per = true;
    private OGLTexture2D texture1, texture2, texture3, texture4, texture5;
    private float dx, dy, ox, oy;
    private float zenit = -45, azimut;
    private OGLTexture2D[] textureCube;
    private float trans, deltaTrans = 0;
    private GLCamera camera;

    public Renderer() {
        super();


        glfwKeyCallback = new GLFWKeyCallback() {
            @Override
            public void invoke(long window, int key, int scancode, int action, int mods) {
                if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE)
                    // We will detect this in our rendering loop
                    glfwSetWindowShouldClose(window, true);
                if (action == GLFW_RELEASE) {
                    trans = 0;
                    deltaTrans = 0;
                }

                if (action == GLFW_PRESS) {
                    switch (key) {
                        case GLFW_KEY_P:
                            per = !per;
                            break;
                        case GLFW_KEY_W:
                            camera.up(0.2f);
                        case GLFW_KEY_S:
                            camera.up(-0.2f);
                        case GLFW_KEY_A:
                            camera.left(0.2f);
                        case GLFW_KEY_D:
                            camera.right(0.2f);
                            deltaTrans = 0.1f;
                            break;
                    }
                }
                switch (key) {
                    case GLFW_KEY_W:
                        camera.up(trans);
                        if (deltaTrans < 0.001f)
                            deltaTrans = 0.001f;
                        else
                            deltaTrans *= 1.02;
                        break;
                    case GLFW_KEY_S:
                        camera.up(-trans);
                        if (deltaTrans < 0.001f)
                            deltaTrans = 0.001f;
                        else
                            deltaTrans *= 1.02;
                        break;

                    case GLFW_KEY_A:
                        camera.left(trans);
                        if (deltaTrans < 0.001f)
                            deltaTrans = 0.001f;
                        else
                            deltaTrans *= 1.02;
                        break;

                    case GLFW_KEY_D:
                        camera.right(trans);
                        if (deltaTrans < 0.001f)
                            deltaTrans = 0.001f;
                        else
                            deltaTrans *= 1.02;
                        break;
                }
            }
        };

        glfwMouseButtonCallback = new GLFWMouseButtonCallback() {

            @Override
            public void invoke(long window, int button, int action, int mods) {
                DoubleBuffer xBuffer = BufferUtils.createDoubleBuffer(1);
                DoubleBuffer yBuffer = BufferUtils.createDoubleBuffer(1);
                glfwGetCursorPos(window, xBuffer, yBuffer);
                double x = xBuffer.get(0);
                double y = yBuffer.get(0);

                mouseButton1 = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS;

                if (button == GLFW_MOUSE_BUTTON_1 && action == GLFW_PRESS) {
                    ox = (float) x;
                    oy = (float) y;
                }
            }

        };

        glfwCursorPosCallback = new GLFWCursorPosCallback() {
            @Override
            public void invoke(long window, double x, double y) {
                if (mouseButton1) {
                    dx = (float) x - ox;
                    dy = (float) y - oy;
                    ox = (float) x;
                    oy = (float) y;
                    zenit -= dy / width * 180;
                    if (zenit > 90)
                        zenit = 90;
                    if (zenit <= -90)
                        zenit = -90;
                    azimut += dx / height * 180;
                    azimut = azimut % 360;
                    camera.setAzimuth(Math.toRadians(azimut));
                    camera.setZenith(Math.toRadians(zenit));
                    dx = 0;
                    dy = 0;
                }
            }
        };

        glfwScrollCallback = new GLFWScrollCallback() {
            @Override
            public void invoke(long window, double dx, double dy) {
                //do nothing
            }
        };
    }


    @Override
    public void init() {
        super.init();
        glClearColor(1, 1, 1, 1);
        textRenderer = new OGLTextRenderer(width, height);

        glEnable(GL_DEPTH_TEST);
        glDisable(GL_CULL_FACE);
        glFrontFace(GL_CW);
        glPolygonMode(GL_FRONT, GL_FILL);
        glPolygonMode(GL_BACK, GL_FILL);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        // surface material setting - diffuse reflection
        float[] mat_dif = new float[]{1, 1, 1, 1};
        // surface material setting - specular reflection
        float[] mat_spec = new float[]{1, 1, 1, 1};
        // surface material setting - ambient reflection
        float[] mat_amb = new float[]{0.1f, 0.1f, 0, 1};


        glMaterialfv(GL_FRONT, GL_AMBIENT, mat_amb);
        glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_dif);
        glMaterialfv(GL_FRONT, GL_SPECULAR, mat_spec);
        glMaterialf(GL_FRONT, GL_SHININESS, 10);
        // gl.glMaterialfv(GL_FRONT, GL_EMISSION, mat);

        // light source setting - diffuse component
        float[] light_dif = new float[]{1, 1, 1, 1};
        // light source setting - ambient component
        float[] light_amb = new float[]{1, 1, 1, 1};
        // light source setting - specular component
        float[] light_spec = new float[]{1, 1, 1, 1};

        glLightfv(GL_LIGHT2, GL_AMBIENT, light_amb);
        glLightfv(GL_LIGHT1, GL_DIFFUSE, light_dif);
        glLightfv(GL_LIGHT1, GL_SPECULAR, light_spec);

        textureCube = new OGLTexture2D[6];

        try {
            texture1 = new OGLTexture2D("textures/asvalt.jfif"); // vzhledem k adresari res v projektu
            texture2 = new OGLTexture2D("textures/road.jpg"); //
            texture3 = new OGLTexture2D("textures/football.jpg");
            texture4 = new OGLTexture2D("textures/apartments7.png"); //dům
            texture5 = new OGLTexture2D("textures/cityHall.jpg");

            textureCube[0] = new OGLTexture2D("textures/skyBox_right.jpg");
            textureCube[1] = new OGLTexture2D("textures/skyBox_left.jpg");
            textureCube[2] = new OGLTexture2D("textures/skyBox_top.jpg");
            textureCube[3] = new OGLTexture2D("textures/images.jfif");
            textureCube[4] = new OGLTexture2D("textures/skyBox_front.jpg");
            textureCube[5] = new OGLTexture2D("textures/skyBox_back.jpg");

        } catch (IOException e) {
            e.printStackTrace();
        }

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);


        camera = new GLCamera();
        camera.setPosition(new Vec3D(0, 0, -30));
        camera.setFirstPerson(true);

        camera.setZenith(-45);
        glMatrixMode(GL_MODELVIEW);

        drawScene();
        skyBox1();

    }

    private void skyBox1() {
        glNewList(1, GL_COMPILE);
        glPushMatrix();
        int size = 40;

        glEnable(GL_TEXTURE_2D);
        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

        textureCube[1].bind(); //-x  (left)
        glBegin(GL_QUADS);
        glTexCoord2f(0.0f, 1.0f);
        glVertex3d(-size, -size, -size);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3d(-size, size, -size);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3d(-size, size, size);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3d(-size, -size, size);
        glEnd();

        textureCube[0].bind();//+x  (right)
        glBegin(GL_QUADS);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3d(size, -size, -size);
        glTexCoord2f(0.0f, 1.0f);
        glVertex3d(size, -size, size);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3d(size, size, size);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3d(size, size, -size);
        glEnd();

        textureCube[3].bind(); //-y bottom
        glBegin(GL_QUADS);
        glTexCoord2f(0.0f, 1.0f);
        glVertex3d(-size, -size, -size);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3d(size, -size, -size);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3d(size, -size, size);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3d(-size, -size, size);
        glEnd();

        textureCube[2].bind(); //+y  top
        glBegin(GL_QUADS);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3d(-size, size, -size);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3d(size, size, -size);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3d(size, size, size);
        glTexCoord2f(0.0f, 1.0f);
        glVertex3d(-size, size, size);
        glEnd();

        textureCube[5].bind(); //-z
        glBegin(GL_QUADS);
        glTexCoord2f(0.0f, 1.0f);
        glVertex3d(size, -size, -size);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3d(-size, -size, -size);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3d(-size, size, -size);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3d(size, size, -size);
        glEnd();

        textureCube[4].bind(); //+z
        glBegin(GL_QUADS);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3d(-size, size, size);
        glTexCoord2f(0.0f, 1.0f);
        glVertex3d(-size, -size, size);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3d(size, -size, size);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3d(size, size, size);
        glEnd();

        glMatrixMode(GL_MODELVIEW);
        glDisable(GL_TEXTURE_2D);
        glPopMatrix();
        glEndList();
    }


    private void drawScene() {
        glEnable(GL_TEXTURE_2D);
        float downPos = -39.9f;

        if (camera.getPosition().getY() < -20)
            camera.setPosition(new Vec3D(camera.getPosition().getX(), -20, camera.getPosition().getY()));

        glPushMatrix();
        glTranslatef(-10, downPos, -60);
        modelHeighBuilding(texture4);
        glMatrixMode(GL_MODELVIEW);
        glPopMatrix();

        glPushMatrix();
        glTranslatef(-5, downPos, -60);
        modelHeighBuilding(texture4);
        glMatrixMode(GL_MODELVIEW);
        glPopMatrix();

        glPushMatrix();
        glTranslatef(0, downPos, -60);
        modelHeighBuilding(texture4);
        glMatrixMode(GL_MODELVIEW);
        glPopMatrix();

        glPushMatrix();
        glTranslatef(5, downPos, -60);
        modelHeighBuilding(texture4);
        glMatrixMode(GL_MODELVIEW);
        glPopMatrix();

        glPushMatrix();
        glTranslatef(10, downPos, -60);
        modelHeighBuilding(texture4);
        glMatrixMode(GL_MODELVIEW);
        glPopMatrix();

        //////////////////

        glPushMatrix();
        glTranslatef(-10, downPos, -55);
        modelHeighBuilding(texture4);
        glPopMatrix();

        glPushMatrix();
        glTranslatef(-5, downPos, -55);
        modelHeighBuilding(texture4);
        glPopMatrix();

        glPushMatrix();
        glTranslatef(0, downPos, -55);
        modelHeighBuilding(texture4);
        glPopMatrix();

        glPushMatrix();
        glTranslatef(5, downPos, -55);
        modelHeighBuilding(texture4);
        glPopMatrix();

        glPushMatrix();
        glTranslatef(10, downPos, -55);
        modelHeighBuilding(texture4);
        glPopMatrix();

        //////////////////

        glPushMatrix();
        glTranslatef(-25, downPos, -50);
        modelLongBuilding(texture4);
        glPopMatrix();

        glPushMatrix();
        glTranslatef(-25, downPos, -45);
        modelLongBuilding(texture4);
        glPopMatrix();

        glPushMatrix();
        glTranslatef(-25, downPos, -40);
        modelLongBuilding(texture4);
        glPopMatrix();

        glPushMatrix();
        glTranslatef(-25, downPos, -35);
        modelLongBuilding(texture4);
        glPopMatrix();

        glPushMatrix();
        glTranslatef(-25, downPos, -30);
        modelLongBuilding(texture4);
        glPopMatrix();

        //////////////////radnice

        glPushMatrix();
        glTranslatef(3, downPos, -37);
        modelCuboid(3, 3, 6, texture5);
        modelCube(3, 3.1f, 6);
        glMatrixMode(GL_MODELVIEW);
        glPopMatrix();

        glPushMatrix();
        glTranslatef(0, downPos, -35.5f);
        modelCuboid(3, 3, 3, texture5);
        modelCube(3, 3.1f, 3);
        glMatrixMode(GL_MODELVIEW);
        glPopMatrix();

        glPushMatrix();
        glTranslatef(-3, downPos, -37);
        modelCuboid(3, 3, 6, texture5);
        modelCube(3, 3.1f, 6);
        glMatrixMode(GL_MODELVIEW);
        glPopMatrix();

        //asvalt
        texture1.bind();
        glBegin(GL_QUADS);
        glPushMatrix();
        glTexCoord2f(1, 0);
        glVertex3f(-20, downPos - 0.01f, -70);
        glTexCoord2f(1, 1);
        glVertex3f(-20, downPos - 0.01f, -29);
        glTexCoord2f(0, 1);
        glVertex3f(15, downPos - 0.01f, -29);
        glTexCoord2f(0, 0);
        glVertex3f(15f, downPos - 0.01f, -70);
        glPopMatrix();
        glEnd();


        //road
        texture2.bind();
        glBegin(GL_QUADS);
        glPushMatrix();
        glTexCoord2f(1, 1);
        glVertex3f(-15, downPos + .01f, -90);//silnice není delší kvůlu textuře
        glTexCoord2f(0, 0);
        glVertex3f(-20, downPos + 0.01f, -90);
        glTexCoord2f(1, 0);
        glVertex3f(-20, downPos + 0.01f, 25);
        glTexCoord2f(1, 1);
        glVertex3f(-15, downPos + 0.01f, 25);
        glPopMatrix();
        glEnd();


        texture2.bind();
        glBegin(GL_QUADS);
        glPushMatrix();
        glTexCoord2f(1, 0);
        glVertex3f(-50, downPos, -29);
        glTexCoord2f(1, 1);
        glVertex3f(55, downPos, -29);
        glTexCoord2f(0, 1);
        glVertex3f(55, downPos, -25);
        glTexCoord2f(0, 0);
        glVertex3f(-50, downPos, -25);
        glPopMatrix();
        glEnd();

        //fotbalové hřiště
        texture3.bind();
        glBegin(GL_QUADS);
        glPushMatrix();
        glTexCoord2f(1, 1);
        glVertex3f(-10, downPos, -20);
        glTexCoord2f(0, 1);
        glVertex3f(10, downPos, -20);
        glTexCoord2f(0, 0);
        glVertex3f(10, downPos, 0);
        glTexCoord2f(1, 0);
        glVertex3f(-10, downPos, 0);
        glPopMatrix();
        glEnd();


        glPushMatrix();
        glMatrixMode(GL_PROJECTION);
        glPopMatrix();
        glDisable(GL_TEXTURE_2D);
    }


    @Override
    public void display() {
        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_TEXTURE_2D);
        glActiveTexture(GL_TEXTURE0);

        glDrawBuffer(GL_FRONT_AND_BACK);

        trans += deltaTrans;

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        if (per)
            gluPerspective(30, width / (float) height, 0.1f, 800.0f);
        else
            glOrtho(-20 * width / (float) height,
                    20 * width / (float) height,
                    -20, 20, 0.1f, 200.0f);

        modelTextures(texture1);
        modelTextures(texture2);
        modelTextures(texture3);

        GLCamera cameraSky = new GLCamera(camera);
        cameraSky.setPosition(new Vec3D());

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glPushMatrix();
        cameraSky.setMatrix();
        glCallList(1);
        glPopMatrix();


        glLoadIdentity();
        camera.setMatrix();
        // nastaveni pozice svetla
        float[] light_position;
        // bod v prostoru
        light_position = new float[]{mouseX - width / 2f, height / 2f - mouseY, 25, 1.0f};
        glLightfv(GL_LIGHT0, GL_POSITION, light_position);

        glPushMatrix();

        // zapnuti svetla a nastaveni modelu stinovani
        glEnable(GL_LIGHTING);
        glEnable(GL_LIGHT0);
        glShadeModel(GL_SMOOTH);

        drawScene();

        glDisable(GL_LIGHTING);
        glDisable(GL_LIGHT0);

        glDisable(GL_DEPTH_TEST);

        String text = this.getClass().getName() + ": [asdw] move, ";

        if (per)
            text += ", [P]ortho ";
        else
            text += ", [p]ersp ";

        text += ", [mouse]look around";

        //create and draw text
        textRenderer.clear();
        textRenderer.addStr2D(3, 20, text);
        textRenderer.addStr2D(width - 90, height - 3, "Jakub Tichý");
        textRenderer.draw();
    }

}
