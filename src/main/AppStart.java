package main;

import main.renderer.LwjglWindow;
import main.renderer.Renderer;

public class AppStart {

    public static void main(String[] args) {
        new LwjglWindow(new Renderer());
    }

}